$(document).ready(function () {

    //scroll top

    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();
        if (scroll >= 200) {
            $(".header").addClass("scrollHeader");
        }
        else {
            $(".header").removeClass("scrollHeader");
        }
    }); //missing );




    //back to top
    $(window).scroll(function () {
        if ($(window).scrollTop() > 100) {
            $("#back-to-top").fadeIn(500);
            $("#back-to-top").css('display', 'flex');
        }
        else {
            $("#back-to-top").fadeOut(500);
        }
    });

    //back to top
    $("#back-to-top").click(function () {
        $('body,html').animate({ scrollTop: 0 }, 500);
        return false;
    });

    $('#humburgerTrigger').click(function () {
        $('body').toggleClass('ismobile');
    });


    $(window).load(function () {
        $('#preloader').delay(350).fadeOut('slow');
        $('body').delay(350).css({
            'overflow': 'visible'
        });
    });
    if (window.matchMedia('(max-width: 1024px)').matches)
    {
        $('.nav-list__item').click(function () {
            var isDisplay = false;
            if ($(this).children('.nav-list__sub-list').css('display') == 'block') {
                isDisplay = true
            }
            $('.nav-list__sub-list').hide();
            if (!isDisplay) {
                $(this).children('.nav-list__sub-list').show();
            }
        })
        $('#nav-menu').change(function() {
            if(!$(this).is(":checked")) {
                $('.nav-list__sub-list').hide();
            }
        });
    }

    $( "#contact-us-form" ).submit(function( event ) {
        var fields = $(this).serializeArray();
        var val = {};
        $(this).empty();
        $.each( fields, function( i, field ) {
            val[field.name] = field.value;
          });
        $.post('http://localhost:3000/api/user/sendmail',  // url
        val, // data to be submit
        function(data, status, xhr) {  
            console.log(status); // success callback function
            $(this).remove();
            if(status == 'success') {
                $('.cotact-us-right-sidebar .contact-success').css({display: 'block'});
            }else {
                $('.cotact-us-right-sidebar .contact-failure').css({display: 'block'});
            }
 
        },
        'json');
        event.preventDefault();
      });
});








