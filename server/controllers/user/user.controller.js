const express = require('express');
const router = express.Router();
const userService = require('./user.service');

// routes
router.post('/sendmail', sendMail);
// router.post('/register', register);

module.exports = router;
//getAllUsers
function sendMail(req, res, next) {
  userService
    .sendMail(req.body, res)
    .then(users => res.json(users))
    .catch(err => next(err));
}
