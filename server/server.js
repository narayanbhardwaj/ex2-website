const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
var cors = require('cors');
const app = express();


const port = process.env.PORT || 3000;

app.use(cors())
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);
app.use(bodyParser.json());

// api routes
app.use('/api/user', require('./controllers/user/user.controller'));


const server = http.createServer(app);

server.listen(port, function () {
  console.log('Listening on port ' + port);
});