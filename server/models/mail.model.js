const mongoose = require('mongoose');
const config = require('../config/config.json');

const mailUserSchema = new mongoose.Schema({
  name: {
    type: String,
    default: '',
    allowNull: false,
    required: false,
  },
  email: {
    type: String,
    default: '',
    allowNull: false,
    required: false
  },
  phone: {
    type: String,
    default: '',
    allowNull: false,
    required: false
  },
  message: {
    type: String,
    default: '',
    allowNull: false,
    required: false
  }
});
module.exports = mongoose.model('mailUser', mailUserSchema);